---
title: "Suicide Data Exploration"
author: "Jack Castiglione and Rahul Rangarajan"
date: "11/20/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

<!-- I set it to not show R code, just outputs. 
That can be toggled through echo= in the first line-->

```{r message=FALSE, warning=FALSE}
read.csv("master.csv") -> data
library(ggplot2)
library(sqldf)
colnames(data) <- c("country", "year", "sex", "age", "suicidesNum", "population", "suicides100K", "c/y", "HDI", "GDP", "GDPpC", "generation")
```

# Questions:

1. For North American, South America, Europe, and Asia, which age demographic has the highest suicide rate? Which has the lowest?

2. Worldwide, what years have the highest suicide rates? Does this change by region?

3. Did specific historical events have impact on the suicide rate in their respective nations?
  Berlin Wall falls - 1989/Germany
  9/11 - 2001/United States

4. Is suicide rate per 100,000 related to national GDP per capita? If so, how strongly and in what manner?

5. How has the disparity in suicide numbers between men and women varied over the available time period?

### Possible Answers

Q1: As there are 5 different age demographics, there are 20 possible answers for a tuple of the demographics with the highest and lowest suicide rates. For each tuple, the response is extremely similar. The only exception is the 15-24 age group in which the primary difference between such demographic and others is the presence of direct education. If that demographic is found to have the highest rate of suicide, direct education is worth considering as a factor. For any other situation, the explanation may not be so easily discernible and therefore the best course of action would be to fund research and also fund facilities and employ professionals to help the people primarily within that demographic who are at risk.

Q2: There are 31 possible answers for this question, each of them being a single year from 1985 to 2016. For each answer, the proper response would be to analyze trends for that year, such as economic down/upturn, and collect resources to be able to properly respond the next time that we encounter a similar set of circumstances.

Q3: For each event, the answer is a yes or no. The significance of this answer is hard to predict but it may be a good indicator of how we should properly respond to events of similar scale in terms of suicide prevention.

Q4: The answer for this question would be a yes or no with accompanying confidence value (r or p). This data could be used to determining where we should funnel worldwide resources for suicide prevention. If it is found that nations with a lower GDP have higher rates of suicide, nations with lower GDPs should be the recipients of more aid. 

Q5: This question wil provide one of three answers: men, women, or neither. Whichever group is found to be more at risk for suicide should be the group we more tailor suicide prevention towards. While there will be suicides from both groups, if there is a statistically significant difference it would likely be within our best interests to focus and tailor resources towards the more represented group.

# Analysis
#### Question X

# Analysis
#### Question X

# Analysis
#### Question X

```{r}
```

```{r}
popByYear <- sqldf('
SELECT sum(population), sum(suicidesNum), year
FROM data
GROUP BY year
')
colnames(popByYear) <- c("population", "suicidesNum", "year")
q2plot <- ggplot(popByYear, aes(x=year, y=suicidesNum/(population/100000))) + geom_point()
q2plot <- q2plot + ggtitle("Worldwide Suicide Rates by Year")
q2plot <- q2plot + xlab("Year") +ylab("Suicides Per 100K")
```

```{r}
```

```{r}
q4data <- sqldf('
SELECT sum(suicides100K), GDPpC, country
FROM data
GROUP BY GDPpC, country
')
colnames(q4data) <- c("suicides100K", "GDPpC", "country")
q4plot <- ggplot(q4data, aes(x=log(GDPpC,2), y=suicides100K)) + geom_point()
```

```{r}
```




